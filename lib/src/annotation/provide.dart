class Provide {
  final bool singleton;

  const Provide({this.singleton = true});
}
