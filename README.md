# dm

Dependency manager like dagger from android.
This package can also create an instance of a class from a reference to the constructor and dependency area

## Example

Code from example/

Create abstract class with annotation @Modules({{you module list}}) for dependency call

        @Modules([CacheModule, DbModule, NetworkModule])
        abstract class Dm {

          static Future<Dm> init() {
            return DmImpl().init();
          }

          @getInstance
          DB db();

          CommonConsumer commonConsumer ();

          MarkInjectConsumer markInjectConsumer();

          WithParameterConsumer withParameterConsumer(String name);
        }

The @Modules annotation contains classes with @provide methods tagged that will be available for calling in other modules and consumers.

        class CacheModule {
          @Provide()
          Cache cacheModule(DB db, Network<String> network) => Cache(db, network);
        }


This function may contain parameters with types that will be taken from other modules

Dependency can get from module or can creating from first or marked @inject constructor in returned class like this

        class MarkInjectConsumer {
          DB db;
          Cache cache;

          MarkInjectConsumer();

          @inject
          MarkInjectConsumer.cached(this.db, this.cache);
        }

